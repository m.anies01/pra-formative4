/// Array buah
let buah = ["Mangga", "Jambu", "Jeruk", "Nanas", "Anggur"];

let forLoop = "==== For Loop ====";
console.log(forLoop);
for(let i = 0; i <buah.length; i++){
    console.log(buah[i]);
}

let forin = "==== For In ====";
console.log(forin);
let x;
for(x in buah){
    console.log(buah[x]);
}

let each = "==== ForEach ====";
console.log(each);
let text ="", spasi =" ";
buah.forEach(function(element){
    text += element + spasi;
});
console.log(text); /// Mencetak dalam satu baris